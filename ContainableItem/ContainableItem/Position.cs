﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContainableItem
{
    public class Position
    {
        private int endColumn;

        public Position(int line, int startColumn, int size)
        {
            StartColumn = startColumn;
            Line = line;
            Size = size;
            SetEndColumn();
        }

        public int Size { get; set; }

        public int Line { get; set; }

        public int StartColumn { get; set; }

        public int GetEndColumn()
        {
            return endColumn;
        }

        public void SetEndColumn()
        {
            endColumn = StartColumn + Size - 1;
        }
    }
}
