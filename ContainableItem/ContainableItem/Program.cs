﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContainableItem
{
    class Program
    {
        static void Main(string[] args)
        {

            ContainableItemsCollection item = new ContainableItemsCollection();

            string[] fileLines = System.IO.File.ReadAllLines(@"ProductFile.txt");
            foreach (var line in fileLines)
            {
                var tabSeparated = line.Split(' ');
                ProductCategory productCategory = new ProductCategory(tabSeparated[2]);
                Product product = new Product(productCategory, tabSeparated[1], Convert.ToInt32(tabSeparated[3]), Convert.ToInt32(tabSeparated[4]), Convert.ToInt32(tabSeparated[0]));
                Position position = new Position(Convert.ToInt32(tabSeparated[5]), Convert.ToInt32(tabSeparated[6]), Convert.ToInt32(tabSeparated[7]));
                ContainableItem containableItem = new ContainableItem(product,position);

                item.Add(null, Convert.ToInt32(tabSeparated[5]), Convert.ToInt32(tabSeparated[6]), Convert.ToInt32(tabSeparated[7]), containableItem);

            }
            item.PrintInstructions();
            bool quit = false;
            int choice = 0;
            while (!quit)
            {
                Console.WriteLine("Enter your choice: ");
                choice = Convert.ToInt32(Console.ReadLine());

                switch (choice)
                {
                    case 0:
                        item.PrintInstructions();
                        break;
                    case 1:
                        item.PrintItems();
                        break;
                    case 2:
                        item.AddItem(choice);
                        break;
                    case 3:
                        item.AddItem(choice);
                        break;
                    case 4:
                        item.RemoveItem();
                        break;
                    case 5:
                        item.SearchItem();
                        break;
                    case 6:
                        quit = true;
                        break;
                }
            }
        }
    }
}
