﻿namespace ContainableItem
{
    public class ContainableItem
    {

        public Product Product { get; set; }
        public Position Position { get; set; }

        public ContainableItem(Product product, Position position)
        {
            Product = product;
            Position = position;
        }

    }
}
