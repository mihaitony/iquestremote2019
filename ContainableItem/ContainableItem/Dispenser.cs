﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContainableItem
{
    public class Dispenser
    {

        public void ProcesingProduct(List<ContainableItem> Items ,int row, int column)
        {
            ContainableItem productToRemove = Items.FirstOrDefault(x => x.Position.Line == row && x.Position.StartColumn <= column && x.Position.GetEndColumn() >= column);

            if (productToRemove == null)
            {
                Console.WriteLine("Product not found in the Vending Machine");
            }
            else
            {
                foreach (ContainableItem product in Items.ToList())
                {
                    if (product.Product.Quantity == 1)
                    {
                        Console.WriteLine("The item is being delivered");
                        product.Product.Quantity -= 1;
                        Console.WriteLine("Product succesfully delivered");
                        Items.Remove(product);
                    }
                    else
                    {
                        if (product.Position.Line == row && product.Position.StartColumn <= column && product.Position.GetEndColumn() >= column)
                        {
                            Console.WriteLine("The item is being delivered");

                            product.Product.Quantity -= 1;
                            System.Threading.Thread.Sleep(2000);

                            Console.WriteLine("Product succesfully delivered!");
                        }
                    }
                }
            }
        }

    }
}
