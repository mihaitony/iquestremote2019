﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContainableItem
{
    public class ContainableItemsCollection
    {
        public List<ContainableItem> Items = new List<ContainableItem>();
        Dispenser dispenser = new Dispenser();

        public void PrintInstructions()
        {
            Console.WriteLine("\nPress: ");
            Console.WriteLine("\t 0 - To print choice options");
            Console.WriteLine("\t 1 - To print all the products from the Vending Machine");
            Console.WriteLine("\t 2 - To add an item to the Vending Machine");
            Console.WriteLine("\t 3 - To add more quantity to an existing item");
            Console.WriteLine("\t 4 - To buy an item from the Vending Machine");
            Console.WriteLine("\t 5 - To search for an item in the Vending Machine");
            Console.WriteLine("\t 6 - To quit the application");
        }

        public void Add( int? id, int? row, int? startColumn, int? size, ContainableItem item = null)
        {
            bool idProduct = Items.Any(x => x.Product.Id == id);
            if (idProduct)
            {
                foreach(ContainableItem product in Items.ToList())
                {
                    if(product.Product.Id == id)
                    {
                        Console.WriteLine("Please enter the amount to add: ");
                        int amount = Convert.ToInt32(Console.ReadLine());
                        product.Product.Capacity(amount);
                        Console.WriteLine("Product added!");
                    }
                }
            }
            else
            {
                ContainableItem exist = Items.FirstOrDefault(x => x.Position.Line == row && x.Position.StartColumn == startColumn && x.Position.Size == size);
                if (exist != null)
                {
                    Console.WriteLine("Product is already in use!");
                }
                else
                {
                    Items.Add(item);
                    Console.WriteLine("Product added!");
                }
            }
        }

        public void AddItem(int choice)
        {

            if (choice == 3)
            {
                Console.WriteLine("Please enter the id of the item: ");
                int id = Convert.ToInt32(Console.ReadLine());

                Add(id, null, null, null, null);
            }
            else
            {

                Console.WriteLine("Please enter the item: ");

                Console.WriteLine("Please enter the id of the item: ");
                int id = Convert.ToInt32(Console.ReadLine());

                Console.WriteLine("The name of the item is: ");
                string name = Console.ReadLine();

                Console.WriteLine("The Category of the item is: ");
                string nameCategory = Console.ReadLine();
                ProductCategory category = new ProductCategory(nameCategory);

                Console.WriteLine("The price of the item is: ");
                double price = Convert.ToDouble(Console.ReadLine());

                Console.WriteLine("The quantity of the item is: ");
                int quantity = Convert.ToInt32(Console.ReadLine());

                Console.WriteLine("Please enter the line where you want to put your item: ");
                int line = Convert.ToInt32(Console.ReadLine());

                Console.WriteLine("Please enter the column where you want to put your item: ");
                int startColumn = Convert.ToInt32(Console.ReadLine());

                Console.WriteLine("Please enter the size of the product");
                int size = Convert.ToInt32(Console.ReadLine());

                Product product = new Product(category, name, price, quantity, id);

                Position position = new Position(line, startColumn, size);

                ContainableItem item = new ContainableItem(product, position);

                Add(null,item.Position.Line, item.Position.StartColumn, item.Position.Size, item);
            }
            
        }

        public void PrintItems()
        {
            Console.WriteLine("There are " + Items.Count() + " items in the VendingMachine");
            foreach (ContainableItem product in Items)
            {
                Console.WriteLine("Id: \t" + product.Product.Id);
                Console.WriteLine("Name: " + product.Product.Name);
                Console.WriteLine("Price: " + product.Product.Price);
                Console.WriteLine("Quantity: " + product.Product.Quantity);
                Console.WriteLine("Line: " + product.Position.Line);
                Console.WriteLine("Column: " + product.Position.StartColumn);
                Console.WriteLine("-----------------------");
            }
        }

        public void SearchByIndexes(int row, int column)
        {
            ContainableItem productToFound = Items.FirstOrDefault(x => x.Position.Line == row && x.Position.StartColumn <= column && x.Position.GetEndColumn() >= column);

            if (productToFound == null)
            {
                Console.WriteLine("Product not found in the Vending Machine");
            }
            else
            {
                foreach (ContainableItem product in Items)
                {
                    if (product.Position.Line == row && product.Position.StartColumn <= column && product.Position.GetEndColumn() >= column)
                    {
                        Console.WriteLine("The item has the following atributes: ");
                        Console.WriteLine("Id: \t" + product.Product.Id);
                        Console.WriteLine("Name: " + product.Product.Name);
                        Console.WriteLine("Price: " + product.Product.Price);
                    }
                }
            }
        }

        public void SearchItem()
        {
            Console.WriteLine("Enter the line of the item you want to search: ");
            int line = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Enter the column of the item you want to search: ");
            int column = Convert.ToInt32(Console.ReadLine());

            SearchByIndexes(line, column);
        }

        public void RemoveItem()
        {
            Console.WriteLine("Enter the line of the item you want to remove: ");
            int line = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Enter the column of the item you want to remove: ");
            int column = Convert.ToInt32(Console.ReadLine());

            dispenser.ProcesingProduct(Items, line, column);
        }
    }
}
