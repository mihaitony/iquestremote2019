﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace VendingMachine
{
    public class ProductCollection
    {
        private List<Product> productList = new List<Product>();


        public void AddProduct(Product product)
        {
            bool exist = productList.Any(x => x.Id == product.Id);
            if (exist)
            {
                Console.WriteLine("Id is already in use");
            }
            else
            {
                productList.Add(product);
            }
        }

        public void PrintProductList()
        {
            Console.WriteLine("You have " + productList.Count() + " items in your ProductCollection");
            foreach (Product item in productList)
            {
                Console.Write($"#{item.Id}. Name: {item.Name} Price: {item.Price} Quantity: {item.Quantity}");
                Console.WriteLine();
            }
        }

        public void RemoveProduct(int position)
        {
            Product productToRemove = productList.FirstOrDefault(x => x.Id == position);

            if (productToRemove != null)
            {
                productList.Remove(productToRemove);
                Console.WriteLine("Product successfully deleted");
            }
            else
            {
                Console.WriteLine("Product not found in the ProductCollection");
            }
        }

        public Product FindProduct(string searchItem)
        {
            Product productFound = productList.FirstOrDefault(x => x.Name == searchItem);
            return productFound;
        }


    }
}
