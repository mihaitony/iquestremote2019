﻿using System;

namespace VendingMachine
{
    public class Program
    {
        static ProductCollection products = new ProductCollection();

        static void Main(string[] args)
        {
            bool quit = false;
            int choice = 0;
            PrintInstructions();
            while (!quit)
            {
                Console.WriteLine("Enter your choice: ");
                choice = Convert.ToInt32(Console.ReadLine());

                switch (choice)
                {
                    case 0:
                        PrintInstructions();
                        break;
                    case 1:
                        products.PrintProductList();
                        break;
                    case 2:
                        AddItem();
                        break;
                    case 3:
                        RemoveItem();
                        break;
                    case 4:
                        SearchItem();
                        break;
                    case 5:
                        quit = true;
                        break;
                }
            }

        }

        public static void PrintInstructions()
        {
            Console.WriteLine("\nPress: ");
            Console.WriteLine("\t 0 - To print choice options");
            Console.WriteLine("\t 1 - To print the ProductCollection list");
            Console.WriteLine("\t 2 - To add an item to the ProductCollection list");
            Console.WriteLine("\t 3 - To remove an item from the ProductCollection list");
            Console.WriteLine("\t 4 - To search for an item in the ProductCollection list");
            Console.WriteLine("\t 5 - To quit the application");
        }

        public static void AddItem()
        {
            Console.WriteLine("Please enter the item: ");

            Console.WriteLine("Please enter id of item: ");
            int id = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("The name of the item is: ");
            string name = Console.ReadLine();

            Console.WriteLine("The price of the item is: ");
            double price = Convert.ToDouble(Console.ReadLine());

            Console.WriteLine("The quantity of the item is: ");
            int quantity = Convert.ToInt32(Console.ReadLine());

            Product product = new Product(id, name, price, quantity);
            products.AddProduct(product);
        }

        public static void RemoveItem()
        {
            Console.WriteLine("Enter item number: ");
            int itemNo = Convert.ToInt32(Console.ReadLine());
            products.RemoveProduct(itemNo);
        }

        public static void SearchItem()
        {
            Console.WriteLine("Enter the name of the item you are searching for: ");
            string searchItem = Console.ReadLine();

            if (products.FindProduct(searchItem) != null)
            {
                Console.WriteLine(products.FindProduct(searchItem));
            }
            else
            {
                Console.WriteLine("Product not found");
            }
        }

    }
}
