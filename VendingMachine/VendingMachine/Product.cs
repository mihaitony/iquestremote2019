﻿namespace VendingMachine
{
    public class Product
    {
        public Product(int idProduct, string productName, double price, int quantity)
        {
            Id = idProduct;
            Name = productName;
            Price = price;
            Quantity = quantity;

        }

        public string Name { get; set; }

        public double Price { get; set; }

        public int Quantity { get; set; }

        public int Id { get; set; }
    }
}
