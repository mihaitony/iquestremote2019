﻿using System;

namespace AnimalKingdom
{
    abstract class Animal
    {
        virtual public void DisplayName()
        {
            Console.WriteLine("I am the parent class");
        }
        virtual public void AnimalSound()
        {
            Console.WriteLine("I have no sound");
        }
    }
}
