﻿using System;

namespace AnimalKingdom
{
    class Snake : Animal
    {
        private readonly string name;
        private Sound sound;

        public Snake(string name, Sound sound)
        {
            this.name = name;
            this.sound = sound;
        }

        private string GetSound()
        {
            return sound.GetSoundType();
        }

        private string GetName()
        {
            return name;
        }

        public override void DisplayName() =>
            Console.WriteLine("I am a Snake, my name is " + GetName());

        public override void AnimalSound() =>
            Console.WriteLine("The sound I make is " + GetSound());
    }
}
