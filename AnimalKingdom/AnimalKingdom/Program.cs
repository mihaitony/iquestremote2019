﻿using System;

namespace AnimalKingdom
{
    class Program
    {
        private static void Main(string[] args)
        {
            for (int i = 1; i <= 3; i++)
            {
                Animal obj = TypeOfAnimal(i);
                obj.DisplayName();
                obj.AnimalSound();
                Console.WriteLine();
            }
        }

        public static Animal TypeOfAnimal(int iterator)
        {
            Sound snake = new Sound("Ssstttt");
            Sound dog = new Sound("HamHam");
            Sound duck = new Sound("MacMac");

            switch (iterator)
            {
                case 1:
                    return new Snake("Serpila", snake);
                case 2:
                    return new Dog("Azor", dog);
                case 3:
                    return new Duck("Bety", duck);
            }
            return null;
        }
    }
}
