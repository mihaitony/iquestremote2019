﻿namespace AnimalKingdom
{
    class Sound
    {
        private readonly string typeOfSound;

        public Sound(string typeOfSound)
        {
            this.typeOfSound = typeOfSound;
        }

        public string GetSoundType()
        {
            return typeOfSound;
        }
    }
}
