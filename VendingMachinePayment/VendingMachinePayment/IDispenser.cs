﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VendingMachinePayment
{
    public interface IDispenser
    {
        void Update(PaymentTerminal payment);
    }
}
