﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VendingMachinePayment
{
    public class Dispenser:IDispenser
    {
        private ContainableItem item;

        public Dispenser(ContainableItem item)
        {
            this.item = item;
        }

        public void DispenserDeliver()
        {
                Console.WriteLine("The item is being delivered");

                item.Product.Quantity -= 1;
                System.Threading.Thread.Sleep(2000);

                Console.WriteLine("Product succesfully delivered!");
        }

        public void Update(PaymentTerminal payment)
        {
            Console.WriteLine("Notified {0} of {1}" + "change to{2:C}", item.Product.Name, item.Product.Category, item.Product.Quantity);
        }

    }
}
