﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VendingMachinePayment
{
    public class ConteinableItemsCollection
    {
        public List<ContainableItem> Items = new List<ContainableItem>();

        public void PrintItems()
        {
            Console.WriteLine("There are " + Items.Count() + " items in the VendingMachine");
            foreach (ContainableItem product in Items)
            {
                Console.WriteLine("Id: \t" + product.Product.Id);
                Console.WriteLine("Name: " + product.Product.Name);
                Console.WriteLine("Price: " + product.Product.Price + "$");
                Console.WriteLine("Quantity: " + product.Product.Quantity);
                Console.WriteLine("Line: " + product.Position.Line);
                Console.WriteLine("Column: " + product.Position.StartColumn);
                Console.WriteLine("-----------------------");
            }
        }

        public void AddItem(ContainableItem item, int  row, int column, int size)
        {

            ContainableItem product = Items.FirstOrDefault(x => x.Position.Line == row && x.Position.StartColumn <= column && x.Position.GetEndColumn() >= column);
            bool checkProducts = Items.Contains(product);

            if (product != null)
            {
                Console.WriteLine("The space is already in use!");
            }
            else
            {
                bool exist = false;
                foreach (ContainableItem iterator in Items)
                {
                    if(checkProducts == false)
                    {
                        break;
                    }
                    else if (product.Product.Id.Equals(iterator.Product.Id))
                    {
                        Console.WriteLine("The following id exists, you must enter an unique id");
                        exist = true;
                        break;
                    }
                }
                if (exist.Equals(false))
                {
                    Items.Add(item);
                    Console.WriteLine("Product added!");
                }
            }
        }

        public void AddToStock(int id, int amount)
        {
            ContainableItem item = Items.FirstOrDefault(x => x.Product.Id == id);

            if (item !=null)
            {
                    item.Product.Capacity(amount);
                    Console.WriteLine($"You have added {amount} to {item.Product.Name}");
            }
        }

        public void SearchByIndexes(int row, int column)
        {
            ContainableItem foundProduct = Items.FirstOrDefault(x => x.Position.Line == row && x.Position.StartColumn <= column && x.Position.GetEndColumn() >= column);

            if (foundProduct == null)
            {
                Console.WriteLine("Product not found in the Vending Machine");
            }
            else
            {
                Console.WriteLine("The item has the following atributes: ");
                Console.WriteLine("Id:\t" + foundProduct.Product.Id);
                Console.WriteLine("Name: " + foundProduct.Product.Name);
                Console.WriteLine("Price:  " + foundProduct.Product.Price + "$");
            }
        }

        public void RemoveByIndexes(int line, int column)
        {
            ContainableItem productToRemove = Items.FirstOrDefault(x => x.Position.Line == line && x.Position.StartColumn <= column && x.Position.GetEndColumn() >= column);

            if (productToRemove == null)
            {
                Console.WriteLine("Product not found in the Vending Machine");
            }
            else
            {
                Console.WriteLine("The product have been succesfully removed");
                Items.Remove(productToRemove);
            }
        }

        public ContainableItem CheckProduct(int line, int column)
        {
            ContainableItem productToRemove = Items.FirstOrDefault(x => x.Position.Line == line && x.Position.StartColumn <= column && x.Position.GetEndColumn() >= column);

            if (productToRemove == null)
            {
                return null;
            }
            else
            {
                return productToRemove;
            }
        }
    }
}
