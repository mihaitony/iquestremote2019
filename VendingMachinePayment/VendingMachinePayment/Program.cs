﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VendingMachinePayment
{
    class Program
    {
        static void Main(string[] args)
        {

            ConteinableItemsCollection item = new ConteinableItemsCollection();

            string[] fileLines = System.IO.File.ReadAllLines(@"ProductFile.txt");
            foreach (string line in fileLines)
            {
                string[] tabSeparated = line.Split(' ');
                ProductCategory productCategory = new ProductCategory (tabSeparated[2]);
                Product product = new Product(productCategory, tabSeparated[1], Convert.ToInt32(tabSeparated[3]), Convert.ToInt32(tabSeparated[4]), Convert.ToInt32(tabSeparated[0]));
                Position position = new Position(Convert.ToInt32(tabSeparated[5]), Convert.ToInt32(tabSeparated[6]), Convert.ToInt32(tabSeparated[7]));
                ContainableItem containableItem = new ContainableItem(product, position);

                item.AddItem(containableItem, Convert.ToInt32(tabSeparated[5]), Convert.ToInt32(tabSeparated[6]), Convert.ToInt32(tabSeparated[7]));
            }

            item.PrintItems();
            ChooseProduct chooseProduct = new ChooseProduct(item);
            chooseProduct.DeliverProduct(2,2);
            item.PrintItems();
        }
    }
}
