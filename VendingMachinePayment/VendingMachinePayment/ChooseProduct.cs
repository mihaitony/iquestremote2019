﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VendingMachinePayment
{
    public class ChooseProduct
    {

        ConteinableItemsCollection item;

        public ChooseProduct(ConteinableItemsCollection item)
        {
            this.item = item;
        }

        public void DeliverProduct(int row, int column)
        {
            ContainableItem productToRemove = item.CheckProduct(row, column);

            if (productToRemove == null)
            {
                Console.WriteLine("Product not found in the Vending Machine");
            }
            else
            {
                if (productToRemove.Product.Quantity == 0)
                {
                    Console.WriteLine("The only operation that you can do is to add, stock is empty");
                }
                else
                {
                    PaymentTerminal payment = new PaymentTerminal(productToRemove);
                    if (payment.CollecMoney().Equals("checked"))
                    {
                        Dispenser dispenser = new Dispenser(productToRemove);
                        dispenser.DispenserDeliver();
                    }
                    else
                    {
                        Console.WriteLine("Fail");
                    }
                }
            }
        }
    }
}

