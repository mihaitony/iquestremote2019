﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VendingMachinePayment
{
    public enum BanknoteType
    {
        None = 0,
        One = 1,
        Five = 5,
        Ten = 10,
        Fifty = 50,
        OneHundred = 100,
    }
}
