﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VendingMachinePayment
{
    public class ProductCategory
    {
        private string Name { get; set; }

        public ProductCategory(string name)
        {
            Name = name;
        }
    }

}
