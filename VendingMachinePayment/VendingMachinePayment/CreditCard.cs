﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VendingMachinePayment
{
    public class CreditCard
    {
        public int CardNumber { get;  set; }
        public int Pin { get;  set; }
        public double Sold { get; set; }

        public CreditCard(int cardNumber, int pin, double sold)
        {
            CardNumber = cardNumber;
            Pin = pin;
            Sold = sold;
        }
    }
}
