﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VendingMachinePayment
{
    public class PaymentBanknote : IPayment
    {

        public double Pay(double price)
        {
            double Sold = 0;
            while (Sold < price)
            {
                Console.WriteLine($"You now have {Sold} in the Vending machine and you have to pay {price}$");
                double previousSold = Sold;
                Sold += AddToSold(Sold);
                if(previousSold == Sold)
                {
                    break;
                }
            }
            return Sold;
        }

        public double AddToSold(double Sold)
        {
            bool quit = false;
            while (!quit)
            {
                Console.WriteLine("Please enter the coins");
                Console.WriteLine("To add 1$ press 1: ");
                Console.WriteLine("To add 5$ press 5: ");
                Console.WriteLine("To add 10$ press 10: ");
                Console.WriteLine("To add 50$ press 50: ");
                Console.WriteLine("To add 100$ press 100: ");
                Console.WriteLine("To quit press 0: ");
                int option = Convert.ToInt32(Console.ReadLine());

                switch (option)
                {
                    case 1:
                        Sold = Convert.ToInt32(BanknoteType.One);
                        quit = true;
                        break;

                    case 5:
                        Sold = Convert.ToInt32(BanknoteType.Five);
                        quit = true;
                        break;

                    case 10:
                        Sold = Convert.ToInt32(BanknoteType.Ten);
                        quit = true;
                        break;

                    case 50:
                        Sold = Convert.ToInt32(BanknoteType.Fifty);
                        quit = true;
                        break;

                    case 100:
                        Sold = Convert.ToInt32(BanknoteType.OneHundred);
                        quit = true;
                        break;

                    case 0:
                        Sold = 0;
                        quit = true;
                        break;
                }
            }
            return Sold;
        } 
    }
}
