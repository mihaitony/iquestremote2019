﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VendingMachinePayment
{
    public interface IPaymentTerminal
    {
        void Add(IDispenser o);
        void Remove(IDispenser o);
        void Notify();
    }
}
