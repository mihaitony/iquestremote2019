﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VendingMachinePayment
{
    public class PaymentCreditCard : IPayment
    {
        public CreditCard Card;

        public PaymentCreditCard(CreditCard card)
        {
            Card = card;
        }

        public bool CheckPin()
        {
            bool pinCheck = false;

            Console.WriteLine("Please enter the pin: ");
            int pin = Convert.ToInt32(Console.ReadLine());

            if (pin.Equals(Card.Pin))
            {
                pinCheck = true;
            }
            return pinCheck;
        }

        public double Pay(double price)
        {
            bool checkPin = CheckPin();
            int check = 0;

            if (checkPin)
            {
                if (price > Card.Sold)
                {
                    Console.WriteLine("You don't have enought money on your card");
                    check = 1;
                }
                else
                {
                    Card.Sold = Card.Sold - price;
                    Console.WriteLine($"Card sold is {Card.Sold}$");
                }
            }
            return check;
        }
    }
}


