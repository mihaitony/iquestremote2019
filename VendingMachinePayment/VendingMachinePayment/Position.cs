﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VendingMachinePayment
{
    public class Position
    {
        private int endColumn;

        public Position(int line, int startColumn, int size)
        {
            StartColumn = startColumn;
            Line = line;
            Size = size;
            SetEndColumn();
        }

        public int Size { get; private set; }

        public int Line { get; private set; }

        public int StartColumn { get; private set; }

        public int GetEndColumn()
        {
            return endColumn;
        }

        private void SetEndColumn()
        {
            endColumn = StartColumn + Size - 1;
        }
    }
}
