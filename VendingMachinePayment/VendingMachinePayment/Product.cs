﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VendingMachinePayment
{
    public class Product
    {
        public Product(ProductCategory category, string name, double price, int quantity, int idProduct)
        {
            Category = category;
            Name = name;
            Price = price;
            Quantity = quantity;
            Id = idProduct;
        }

        public ProductCategory Category { get;private set; }

        public string Name { get; set; }

        public double Price { get; set; }

        public int Quantity { get; set; }

        public int Id { get; private set; }

        public int Capacity(int amount)
        {
            if (amount > 0)
            {
                int capacity = Quantity + amount;

                if (capacity <= 100)
                {
                    Quantity = capacity;
                }
                else
                {
                    Console.WriteLine("The amount you have entered is to much");
                    Console.WriteLine($"You only have {100 - Quantity} available spaces for {Name}");
                    Console.WriteLine("Please enter a new amount: ");
                    amount = Convert.ToInt32(Console.ReadLine());
                    Capacity(amount);
                }
            }
            else
            {
                Console.WriteLine("The amount you have entered is not available");
            }
            return Quantity;
        }
    }
}
