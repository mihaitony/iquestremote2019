﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VendingMachinePayment
{
    public class PaymentCoins : IPayment
    {
        public double Pay(double price)
        {
            double Sold = 0;
            while (Sold < price)
            {
                Console.WriteLine($"You now have {Sold} in the Vending machine and you have to pay {price}$");
                double previousSold = Sold;
                Sold += AddToSold(Sold);

                if (previousSold == Sold)
                {
                    break;
                }
            }
            return Sold;
        }

        public double AddToSold(double Sold)
        {
            bool quit = false;
            while (!quit)
            {
                Console.WriteLine("Please enter the coins");
                Console.WriteLine("To add 0.50$ press 5: ");
                Console.WriteLine("To quit press 0: ");
                int option = Convert.ToInt32(Console.ReadLine());

                switch (option)
                {
                    case 5:
                        Sold = (Convert.ToInt32(CoinType.Five) * 0.1) ;
                        quit = true;
                        break;
                    case 0:
                        Sold = 0;
                        quit = true;
                        break;
                }
            }
            return Sold;
        }
    }
}
