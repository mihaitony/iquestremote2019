﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VendingMachinePayment
{
    public class PaymentTerminal: IPaymentTerminal
    {
        static CreditCard Card = new CreditCard(12331231, 1234, 110);

        IPayment payment = new PaymentCreditCard(Card);

        private List<IDispenser> _observers = new List<IDispenser>();
        public ContainableItem Item;


        public PaymentTerminal(ContainableItem Item)
        {
            this.Item = Item;
        }

        public string CollecMoney()
        {
            double Sold = payment.Pay(Item.Product.Price);
            return GiveChange(Sold);

        }

        public string GiveChange(double Sold)
        {
            int noOfCoins = 0;
            string statusPayment = "unchecked";
            Console.WriteLine(Sold);

            if (Sold.Equals(1))
            {
                statusPayment = "unchecked";
            }
            else if (Sold.Equals(0))
            {
                statusPayment = "checked";
            }
            else if (Sold < Item.Product.Price)
            {
                noOfCoins = (Convert.ToInt32(Sold) * 10) / Convert.ToInt32(CoinType.Five);
                Console.WriteLine("Return money with coins of 0.50$");
                Console.WriteLine($"Total coins: {noOfCoins}");
                statusPayment = "unchecked";
            }
            else if(Sold > Item.Product.Price)
            {
                double moneyChange = Sold - Item.Product.Price;
                noOfCoins = (Convert.ToInt32(moneyChange) * 10) / Convert.ToInt32(CoinType.Five);
                Console.WriteLine("Give back change with coins of 0.50$");
                Console.WriteLine($"Total coins: {noOfCoins}");
                statusPayment = "checked";
            }
            return statusPayment;
        }

        public void Add(IDispenser observer)
        {
            _observers.Add(observer);
        }

        public void Remove(IDispenser observer)
        {
            _observers.Remove(observer);
        }

        public void Notify()
        {
            foreach(IDispenser observer in _observers)
            {
                observer.Update(this);
            }
        }

        public string GetPayment()
        {
            return CollecMoney();
        }
    }
}
